import random
import pygame
import itertools
from algs import *


color_palette_iterator = itertools.cycle([pygame.Color(
    50, 50, 50), pygame.Color(
    100, 100, 100), pygame.Color(
    150, 150, 150), pygame.Color(
    200, 200, 200)])

array_sizes_iterator = itertools.cycle([10, 50, 100, 200, 400])

sort_timer_millis_iterator = itertools.cycle([1, 5, 10, 20, 40])

screen_size = pygame.Vector2(800, 600)
sort_timer_id = pygame.USEREVENT + 0
font_title = 'comic-sans'
font_size = 22


def map_range(range_in, range_out, val):
    (a1, a2), (b1, b2) = range_in, range_out
    return b1 + ((val - a1) * (b2 - b1) / (a2 - a1))


class App:
    def __init__(self):
        pygame.init()

        self.get_next_array()
        self.the_swap_indices = (-1, -1)
        self.the_sort_timer_millis = next(sort_timer_millis_iterator)

        self.is_running = False
        self.is_sorting = False
        self.is_ascending = True
        self.the_algorithm = next(algorithms_iterator)
        self.the_algorithm_generator = None

        self.the_screen = pygame.display.set_mode(screen_size)
        pygame.display.set_caption('Sorting Algorithm Visualizer')

        self.the_font = pygame.font.SysFont(font_title, font_size)

    def __del__(self):
        pygame.quit()

    def get_next_array(self):
        self.the_size = next(array_sizes_iterator)
        self.the_array = [*range(self.the_size)]
        self.the_min = min(self.the_array)
        self.the_max = max(self.the_array)
        self.the_element_width = screen_size.x / len(self.the_array)
        self.the_colors = [next(color_palette_iterator)
                           for _ in self.the_array]
        random.shuffle(self.the_array)

    def get_info_text(self):
        algorithm_text = self.the_algorithm.__name__.replace('_', ' ').title()
        ascending_text = 'Ascending' if self.is_ascending else 'Descending'
        size_text = str(self.the_size)
        speed_text = str(self.the_sort_timer_millis)
        sorting_text = 'Sorting' if self.is_sorting else 'Ready'
        return (f'A: {algorithm_text} - '
                f'I: {ascending_text}\n'
                f'W: Size {size_text} - '
                f'S: Speed {speed_text} ms\n'
                f'R: Randomize - '
                f'SPACE: {sorting_text}')

    def render_text(self):
        lines = self.get_info_text().split('\n')
        surface_lines = [
            self.the_font.render(
                line, True, (255, 255, 255)) for line in lines]
        y = 0
        for line in surface_lines:
            self.the_screen.blit(line, (10, y))
            y += 40

    def handle_events(self):
        for ev in pygame.event.get():
            if ev.type == pygame.QUIT:
                self.is_running = False
            if ev.type == pygame.KEYDOWN:
                if ev.key == pygame.K_ESCAPE:
                    self.is_running = False
                if self.is_sorting:
                    continue

                if ev.key == pygame.K_a:
                    self.the_algorithm = next(algorithms_iterator)
                if ev.key == pygame.K_i:
                    self.is_ascending = not self.is_ascending
                if ev.key == pygame.K_w:
                    self.get_next_array()
                if ev.key == pygame.K_s:
                    self.the_sort_timer_millis = next(
                        sort_timer_millis_iterator)
                if ev.key == pygame.K_r:
                    random.shuffle(self.the_array)
                if ev.key == pygame.K_SPACE:
                    self.the_algorithm_generator = self.the_algorithm(
                        self.the_array, self.is_ascending)
                    pygame.time.set_timer(
                        sort_timer_id, self.the_sort_timer_millis)
                    self.is_sorting = True

            if ev.type == sort_timer_id:
                try:
                    self.the_swap_indices = next(self.the_algorithm_generator)
                except StopIteration as ex:
                    self.is_sorting = False
                    pygame.time.set_timer(sort_timer_id, 0)
                    self.the_swap_indices = (-1, -1)

    def render(self):
        self.the_screen.fill((0, 0, 0))
        for i in range(len(self.the_array)):
            element = self.the_array[i]
            color = self.the_colors[i]
            if self.is_sorting:
                (s1, s2) = self.the_swap_indices
                if i == s1:
                    color = pygame.Color(255, 0, 0)
                if i == s2:
                    color = pygame.Color(0, 255, 0)
            element_height = map_range(
                (self.the_min, self.the_max), (10, screen_size.y - 140), element)
            pygame.draw.rect(
                self.the_screen,
                color,
                pygame.Rect(i * self.the_element_width, screen_size.y - element_height, self.the_element_width, element_height))
        self.render_text()
        pygame.display.update()

    def Run(self):
        clock = pygame.time.Clock()
        self.is_running = True
        while self.is_running:
            clock.tick(60)
            self.handle_events()
            self.render()


if __name__ == "__main__":
    app = App()
    app.Run()
