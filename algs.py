import itertools


def bubble_sort(elements, is_ascending=True):
    for n in range(len(elements) - 1, 0, -1):
        for i in range(n):
            if (is_ascending and elements[i] > elements[i + 1]) or (
                    not is_ascending and elements[i] < elements[i + 1]):
                elements[i], elements[i + 1] = elements[i + 1], elements[i]
                yield (i, i + 1)
    return False


def selection_sort(elements, is_ascending=True):
    for i in range(len(elements)):
        min_idx = i
        for j in range(i + 1, len(elements)):
            if (is_ascending and elements[min_idx] > elements[j]) or (
                    not is_ascending and elements[min_idx] < elements[j]):
                min_idx = j

        elements[i], elements[min_idx] = elements[min_idx], elements[i]
        yield (i, min_idx)
    return False


def insertion_sort(elements, is_ascending=True):
    for i in range(1, len(elements)):
        key = elements[i]
        j = i - 1
        while j >= 0 and ((is_ascending and key < elements[j]) or (
                not is_ascending and key > elements[j])):
            elements[j + 1] = elements[j]
            yield(j + 1, j)
            j -= 1
        elements[j + 1] = key
        yield(j + 1, key)
    return False


algorithms_iterator = itertools.cycle(
    [bubble_sort, selection_sort, insertion_sort])
